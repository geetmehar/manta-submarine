You can try the app in following way.

1. Open submarine.html and open jacksparrow.html

2. Register a name in submarine.html
- If the name doesn't exist then you can see the submarine name gets added to jacksparrow page along with hide button

- And, submarine page UI adds hide button

- If the name exists then error is shown

3. Refresh submarine page to see registration page again. You can add as many submarine names as you want.

4. I have not finished the hide functionality as I didn't have enough time but given more time, I can finish that as well.