console.log('Page loading')

// ***Global variables are defined here
const clientUUID = PubNub.generateUUID();

const theChannel = 'Pirates';

const theEntry = 'Submarine';

const pubnub = new PubNub({
    publishKey: 'pub-c-dd9d8d6b-8362-4909-9da4-6a1fc36b68d9',
    subscribeKey: 'sub-c-b79ee46e-de3b-11ea-9b19-4e2692a0ef15',
    uuid: clientUUID
});
// End of defining global variables


// ***Functions are defined from here


// When submarine wants to send message to jack sparrow
  submitUpdate = function(anEntry, anUpdate) {
    pubnub.publish({
      channel : theChannel,
      message : {'entry' : anEntry, 'update' : anUpdate}
    },
    function(status, response) {
      if (status.error) {
        console.log(status)
      }
      else {
        console.log('timetoken: ' + response.timetoken);

        $('#inputName').val("");
      }
    });
  };
// End of submarine wants to send message to jack sparrow

// ***End of defining functions




// ***Code to run when webpage loads
$(document).ready(function(){

	// Pubnub message listener
	pubnub.addListener({
	    message: function(event) {

	    if(event.message.entry == 'Jack') {
	    	 console.log('[MESSAGE: received]',
		        event.message.entry + ': ' + event.message.update);
		    }

		    if(event.message.update === 'success'){

		    	// Name registered successfully

		    	$('.registration').css('display','none')

		    	$('.hideButton').css('display', 'block')

		    }

		    if(event.message.update === 'failure'){

		    	// Name already exists, try another
		    	$('.errorMessage').css('display', 'block')

		    	$('.errorMessage').text('This name already exists, Please choose another.')
		    }

	    }
	     
	});

	// Subscribe to pubnub channel
	pubnub.subscribe({
	    channels: ['Pirates'],
	    withPresence: true
	});


})
// ***End of code to run when webpage loads



// ***Trigger events are defined from here

// When user enters name to register it.
$('html').on('click', '#registerName', function(){

	// Check if name is alphanumeric 
	var val = $('#inputName').val();
	console.log(val) 

    if (/\d/.test(val) && /[a-zA-Z]/.test(val)) {

    	console.log('true')

    	// It is alphanumeric input

    	$('.errorMessage').css('display', 'none')

    	$('.hideButton').css('display', 'none')

    	submitUpdate(theEntry, $('#inputName').val())


    } else {

    	console.log('false')

    	// It is not alphanumberic input
    	alert('Please enter AplhaNumeric Name for the submarine.')
    }

	
})


// ***End of defining trigger events