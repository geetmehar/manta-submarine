console.log('Page loading')

// ***Global variables are defined here
const clientUUID = PubNub.generateUUID();

const theChannel = 'Pirates';

const theEntry = 'Jack';

const pubnub = new PubNub({
    publishKey: 'pub-c-dd9d8d6b-8362-4909-9da4-6a1fc36b68d9',
    subscribeKey: 'sub-c-b79ee46e-de3b-11ea-9b19-4e2692a0ef15',
    uuid: clientUUID
});

SubmarineModel = Backbone.Model.extend({

	default: {

		name: 'unknown'
	}
})

SubmarineCollection = Backbone.Collection.extend({

	model: SubmarineModel
})

var SubmarineCollection_temp = new SubmarineCollection()
// End of defining global variables


// ***Functions are defined from here

// When jack wants to send message to submarine
  submitUpdate = function(anEntry, anUpdate) {
    pubnub.publish({
      channel : theChannel,
      message : {'entry' : anEntry, 'update' : anUpdate}
    },
    function(status, response) {
      if (status.error) {
        console.log(status)
      }
      else {
        console.log('timetoken: ' + response.timetoken);

      }
    });
  };
// End of jack wants to send message to submarine


// Add submarine name to the list
addSubmarine = function(name){

	$('.submarineList').append('<h3>' + name + '</h3><button class="btn btn-primary">Hide</button>' )
}

// End of add sumarine name to the list


// ***End of defining functions




// ***Code to run when webpage loads
$(document).ready(function(){

	// Listen to submarine messages
	pubnub.addListener({
	    message: function(event) {

		    if(event.message.entry == 'Submarine'){

		    	console.log(SubmarineCollection_temp)

		    	console.log(SubmarineCollection_temp.toJSON())

		    	// Check against backbone list and accordingly send success or failure message to submarine

		    	var existingNames = SubmarineCollection_temp.toJSON()


		    	if(existingNames.length > 0){

		    		console.log(existingNames.length)

		    		// Check if name exists already
		    		var i = 0;
		    		while(i<existingNames.length){

		    			console.log(existingNames[i].name)

		    			if(existingNames[i].name === event.message.update){

		    				console.log('failure')

		    				submitUpdate(theEntry,'failure')

		    				break;

		    			}

		    			i++

		    			if(i == existingNames.length){

		    				var submarineName = new SubmarineModel({ name: event.message.update })

					      	SubmarineCollection_temp.add(submarineName)

					      	console.log(SubmarineCollection_temp)
					      	
					      	console.log(SubmarineCollection_temp.toJSON())

		    				console.log('success')

		    				submitUpdate(theEntry,'success')

		    				addSubmarine(event.message.update)

		    			}


		    		}


		    	} else {

  				      var submarineName = new SubmarineModel({ name: event.message.update })

				      SubmarineCollection_temp.add(submarineName)

				      console.log(SubmarineCollection_temp)
				      console.log(SubmarineCollection_temp.toJSON())

				      submitUpdate(theEntry,'success')

				      addSubmarine(event.message.update)

		    	}

		    	console.log('[MESSAGE: received]',
		        event.message.entry + ': ' + event.message.update);


		    }

	    }
	});

	// Subscribe to pubnub channel
	pubnub.subscribe({
	    channels: ['Pirates'],
	    withPresence: true
	});



})
// ***End of code to run when webpage loads



// ***Trigger events are defined from here


// ***End of defining trigger events